import pandas as pd
import pytest

from funcbox import str_to_date_with_pandas


@pytest.mark.unit
def test_function_str_to_date_with_pandas():
    """Tests the str_to_date_with_pandas function to check if it is
    correctly converting date strings in a pandas DataFrame.

    This test creates a DataFrame with two columns of date strings,
    some of which are invalid dates. The str_to_date_with_pandas
    function is called to convert the strings into dates. The test then
    checks if the function correctly filled cells with invalid dates
    with an error message.

    The assertions check if:
    - The cell in row 1 of the 'column_date_one' column contains the
      expected error message for an invalid date.
    - The cell in row 3 of the 'column_date_two' column contains the
      expected error message for an invalid date.

    Returns:
        None
    """
    df = pd.DataFrame(
        {
            'column_date_one': [
                '2000-01-01',
                '1990-02-32',
                '1985-03-15',
                '1970-31-16',
                '1975-05-05',
            ],
            'column_date_two': [
                '2000/01/01',
                '1990/02/32',
                '1985/03/15',
                '1970/32/16',
                None,
            ],
        }
    )
    str_to_date_with_pandas(
        df, [('column_date_one', '%Y-%m-%d'), ('column_date_two', '%d/%m/%Y')]
    )
    assert (
        df.loc[1, 'column_date_one'] == 'ERROR: 1990-02-32 is an invalid date'
    )
    assert (
        df.loc[3, 'column_date_two'] == 'ERROR: 1970/32/16 is an invalid date'
    )
