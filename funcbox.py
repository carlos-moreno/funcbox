import pandas as pd


def str_to_date_with_pandas(df, columns):
    """Convert strings to dates in a pandas DataFrame.

    This function takes a pandas DataFrame and a list of tuples. Each
    tuple contains the name of a column in the DataFrame and a date
    format as a string. The function converts the strings in the
    specified column to dates using the provided format. If the
    conversion fails, the function fills the cell with an error
    message.

    Parameters:
        df (pandas.DataFrame): The DataFrame to be modified.
        columns (list[tuple[str, str]]): A list of tuples. Each tuple
                                         contains the name of a column
                                         in the DataFrame and a date
                                         format as a string.

    Returns:
        None
    """
    for column, fmt in columns:
        mask = df[column].notnull()
        df.loc[mask, column] = pd.to_datetime(
            df.loc[mask, column], format=fmt, errors='coerce'
        ).fillna('ERROR: ' + df[column].astype(str) + ' is an invalid date')
